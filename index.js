const networkAction = (opts) => (dispatch, getState) => {
  // Grab the access token if any
  const state = getState()
  const token = opts.token || (state.user && state.user.access_token)

  // Initialize the fetch options object
  const fetchOpts = opts.fetchOptions || {}
  if (!fetchOpts.headers) fetchOpts.headers = {}

  // Process the options provided by the user
  if (opts.body) {
    // If a body is provided, we default to 'POST'
    fetchOpts.method = fetchOpts.method || 'POST'

    // Body process hook
    if (opts.processBody) opts.body = opts.processBody(opts.body, state)

    // Check the body type
    if (opts.body.toString() === '[object FormData]') {
      // FormData: no need to set anything. Just pass it through.
      fetchOpts.body = opts.body
    } else if (typeof opts.body === 'string') {
      // A string: probably a form-urlencoded.
      fetchOpts.body = opts.body
      if (!fetchOpts.headers['Content-Type']) {
        fetchOpts.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      }
    } else if (typeof opts.body === 'object') {
      // Something else: wild guess, but probably a JSON.
      fetchOpts.body = JSON.stringify(opts.body)
      if (!fetchOpts.headers['Content-Type']) {
        fetchOpts.headers['Content-Type'] = 'application/json'
      }
    }
  }

  // If we detected a token, use it
  if (token) {
    fetchOpts.headers['Authorization'] = 'Bearer ' + token
  }

  // If running in Universal Scripts on the server,
  // passthrough any cookies (client to server)
  if (state.req && state.req.headers) {
    fetchOpts.timeout = opts.timeout || 10000
    fetchOpts.headers['X-Forwarded-For'] = state.req.ip
    fetchOpts.headers['Cookie'] = state.req.headers.cookie
  }

  // Process request hook
  if (opts.processRequest) opts.processRequest(fetchOpts, state)

  // Get a timestamp for the request
  const ts = Date.now()

  // Dispatch the 'loading' action
  const action = {
    type: opts.action + '/loading',
    ts
  }
  if (opts.meta) action.meta = opts.meta
  dispatch(action)

  // Prepare the url
  let url = (opts.baseUrl || '') + opts.url

  if (opts.params) {
    url += '?' + formEncode(opts.params)
  }

  // Start the request
  return fetch(url, fetchOpts)
    .then((response) => {
      const resType = response.headers.get('Content-Type')
      const isJson = resType && resType.startsWith('application/json')
      const valuePromise = isJson ? response.json() : response.text()

      return valuePromise.then((data) => {
        if (opts.processResponse) data = opts.processResponse(data, response)
        if (!response.ok) {
          const e = new Error(data.error || response.statusText || 'Unknown error')
          e.data = data
          e.status = response.status
          throw e
        }
        const action = {
          type: opts.action + '/success',
          status: response.status,
          payload: data,
          ts
        }
        if (opts.meta) action.meta = opts.meta
        dispatch(action)
        // If running in Universal Scripts on the server,
        // passthrough any cookies (server to client)
        if (state.req && response.headers) {
          let cookies = response.headers.getAll('Set-Cookie')
          if (cookies.length) {
            data.__serverDirectives = { cookies: [] }
            cookies.forEach((cookie) => {
              const cookieObj = cookieParse(cookie)
              if (cookieObj.name[0] !== '_') {
                data.__serverDirectives.cookies.push(cookieObj)
              }
            })
          }
        }
        return data
      })
    })
    .catch((e) => {
      const action = {
        type: opts.action + '/error',
        error: e.message,
        ts
      }
      if (opts.meta) action.meta = opts.meta
      if (e.status) action.status = e.status
      if (e.data) action.data = e.data
      dispatch(action)
      if (!opts.noReject) throw e
    })
}

networkAction.withDefaults = (defaults) => (opts) => {
  const mixedOpts = { ...defaults, ...opts }
  // Mix fetchOptions instead of straight overriding
  if (defaults.fetchOptions) {
    mixedOpts.fetchOptions = {
      ...defaults.fetchOptions,
      ...opts.fetchOptions
    }
    // Headers need mixing too
    if (defaults.fetchOptions.headers) {
      mixedOpts.fetchOptions.headers = {
        ...defaults.fetchOptions.headers,
        ...(opts.fetchOptions && opts.fetchOptions.headers)
      }
    }
  }
  return networkAction(mixedOpts)
}

const cookieParse = (cookie) => {
  const cookieEntry = {}
  const splitOnce = (s, k) => {
    let idx = s.indexOf(k)
    if (idx === -1) idx = s.length
    return [s.substring(0, idx), s.substring(idx + 1)]
  }
  const params = cookie.split(';').map((x) => splitOnce(x.trim(), '='))
  const keyvalue = params.shift()
  cookieEntry.name = keyvalue[0]
  cookieEntry.value = keyvalue[1]
  params.forEach(([key, value]) => {
    cookieEntry[key.toLowerCase()] = value || true
  })
  if (cookieEntry.expires) cookieEntry.expires = new Date(cookieEntry.expires)
  return cookieEntry
}

const formEncode = (params) => params ? Object.entries(params)
  .map(([key, value]) => {
    if (!key || value == null) return ''
    return encodeURIComponent(key) + '=' + encodeURIComponent(value)
  }).join('&') : ''


networkAction.reducer = (actionInput) => (state, action) => {
  const actionType = actionInput instanceof Array
    ? actionInput.find((elem) => action.type.startsWith(elem + '/'))
    : actionInput

  if (typeof actionType !== 'string') return state || null

  const { meta, data } = action
  if (action.type === actionType + '/loading') {
    return { isLoading: true, oldValue: state && state.value, ...(meta && {meta}) }
  } else if (action.type === actionType + '/success') {
    return { isSuccess: true, value: action.payload, ...(meta && {meta}) }
  } else if (action.type === actionType + '/error') {
    return {
      isError: true,
      error: action.payload || action.error,
      ...(meta && {meta}),
      ...(data && {data})
    }
  } else {
    return state || null
  }
}

networkAction.keyedReducer = (actionType, key) => (state, action) => {
  if (!action.type.startsWith(actionType)) return state || {}
  const thisKey = key instanceof Function ? key(action) : action.meta[key]

  if (action.type === actionType + '/loading') {
    return { ...state, [thisKey]: { isLoading: true, oldValue: state && state.value }}
  } else if (action.type === actionType + '/success') {
    return { ...state, [thisKey]: { isSuccess: true, value: action.payload }}
  } else if (action.type === actionType + '/error') {
    return { ...state, [thisKey]: { isError: true, error: action.payload || action.error } }
  } else {
    return state || {}
  }
}

export default networkAction
